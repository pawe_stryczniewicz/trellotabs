**EDIT:**
**Dodano Retrofit, odswieżanie widoku, refactor kodu, podział na package'e.**

**SourceTree i Git jest dla mnie nowym narzędziem, więc nie wiem czy moje repo po commicie i push'u wygląda tak jak powinno (w razie gdyby coś było nie tak wystarczy pobrać pliki z commitu "Retrofit commit")**



---------
Program zawiera kilka błędów i wymagałby jeszcze sporo pracy, ale z grubsza prezentuje wykorzystanie Trello API.

Krótki opis:
Za pomocą aplikacji Android można zarządać tablicą Trello znajdującą się pod adresem podanym poniżej:

Link do tablicy:
https://trello.com/b/tQTRTVDF/my-board



Spełnione wymagania:
 - wykorzystano API https://trello.com/docs
 - aplikacja umozliwia zarzadanie kartami (dodawanie, usuwanie)
 - karty można dowolnie przenosić między listami
 - (Opcjonalne) Karty można dodawać tylko do listy "To do"