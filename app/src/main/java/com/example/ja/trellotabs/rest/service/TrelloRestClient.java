package com.example.ja.trellotabs.rest.service;

import com.example.ja.trellotabs.MainActivity;
import com.example.ja.trellotabs.ViewPagerAdapter;
import com.example.ja.trellotabs.rest.model.Card;
import com.example.ja.trellotabs.rest.model.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EBean
public class TrelloRestClient {

    static ArrayList<String> toDoCardsArray = new ArrayList<>();
    static ArrayList<String> doingCardsArray = new ArrayList<>();
    static ArrayList<String> doneCardsArray = new ArrayList<>();

    static String API = "https://api.trello.com";
    static String idToDo = "5586d630b51a32a3375c7bc0";
    static String idDoing = "5586d6324cbc8a194be79707";
    static String idDone = "5586d633f14bea770fc1b14c";

    static String developerKey = "9d5813c2dc048eb3d81fb746d79bd584";
    static String authToken = "f4e40689e6eb8dfea43ac824451a2b5f2ef238be15bc469c854d7f82070f097f";


    private MainActivity mainActivity;
    private ViewPagerAdapter viewPagerAdapter;

    RestAdapter restAdapter;
    TrelloApi trelloApi;

    public TrelloRestClient(){
        restAdapter = new RestAdapter.Builder().setEndpoint(API).build();
        trelloApi = restAdapter.create(TrelloApi.class);
    }


    public void injectObjects(MainActivity mainActivity){
        this.mainActivity = mainActivity;

    }

    @Background
    public void postAction(String cardName){

        Card card = new Card();
        card.setName(cardName);

        trelloApi.setCard(card, new Callback<Card>() {
            @Override
            public void success(Card card, Response response) {
                refreshList(idToDo);
            }

            @Override
            public void failure(RetrofitError error) {
                //TODO log failure
            }
        });

    }

    @Background
    public void putAction(int selectedId, int srcTab, int destTab){

        final String destListId = destTab == 0 ? idToDo : (destTab == 1 ? idDoing : idDone);
        String cardId = toDoCardsArray.get(selectedId - 1);

        trelloApi.moveCard(cardId, destListId, new Callback<Card>() {
            @Override
            public void success(Card card, Response response) {
                refreshList(destListId);
            }

            @Override
            public void failure(RetrofitError error) {
                //TODO log failure
            }
        });

    }

    @Background
    public void deleteAction(int selectedId, int srcDeleteTab){

        String deleteCard = API + "/1/cards/" + toDoCardsArray.get(selectedId-1)  + "?key=" + developerKey + "&token=" + authToken;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(deleteCard);

        toDoCardsArray.remove(selectedId - 1);

        mainActivity.removeCardView(selectedId - 1, srcDeleteTab);

    }

    public void refreshList(String listNr, final ViewPagerAdapter viewPagerAdapter) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();

        TrelloApi trelloApi = restAdapter.create(TrelloApi.class);

        this.viewPagerAdapter = viewPagerAdapter;

        trelloApi.getList(listNr, new Callback<List>() {
            @Override
            public void success(List list, Response response) {
                viewPagerAdapter.fillList(list);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void refreshList(String listNr) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();

        TrelloApi trelloApi = restAdapter.create(TrelloApi.class);

        trelloApi.getList(listNr, new Callback<List>() {
            @Override
            public void success(List list, Response response) {
                mainActivity.updateToDoUi(list);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

}
