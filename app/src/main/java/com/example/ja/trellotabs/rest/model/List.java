package com.example.ja.trellotabs.rest.model;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;

public class List {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private java.util.List<Card> cards = new ArrayList<Card>();

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The cards
     */
    public java.util.List<Card> getCards() {
        return cards;
    }

    /**
     *
     * @param cards
     * The cards
     */
    public void setCards(java.util.List<Card> cards) {
        this.cards = cards;
    }

}