package com.example.ja.trellotabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.example.ja.trellotabs.fragments.TabBasic;
import com.example.ja.trellotabs.fragments.TabDoing;
import com.example.ja.trellotabs.fragments.TabDone;
import com.example.ja.trellotabs.fragments.TabToDo;
import com.example.ja.trellotabs.rest.model.Card;
import com.example.ja.trellotabs.rest.model.List;
import com.example.ja.trellotabs.rest.service.TrelloApi;
import com.example.ja.trellotabs.rest.service.TrelloRestClient;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {


    private final MainActivity mainActivity;
    private final TrelloRestClient trelloRestClient;
    CharSequence Titles[];
    int NumOfTabs;
    Fragment myFragment;
    private Map<Integer, Fragment> mPageReferenceMap = new HashMap<Integer, Fragment>();


    public ViewPagerAdapter(MainActivity mainActivity, FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumOfTabs = mNumbOfTabsumb;
        this.mainActivity = mainActivity;
        this.trelloRestClient = new TrelloRestClient();
//        trelloRestClient.injectViewPagerAdapter(this);

    }

    @Override
    public Fragment getItem(final int position) {

        //Big problem here - getItem is not always called (ViewPagerAdapter should be managed by getChildFragmentManager() instead of getSupportFragmentManager()

        String listNr = "";
        if(position==0){
            myFragment = new TabToDo();
            listNr = "5586d630b51a32a3375c7bc0"; //for some reason variable listNumber is not memorized in TabToDo (TabBasic) instance
        }
        else if (position==1){
            myFragment = new TabDoing();
            listNr = "5586d6324cbc8a194be79707";
        }
        else {
            myFragment = new TabDone();
            listNr = "5586d633f14bea770fc1b14c";
        }


        trelloRestClient.refreshList(listNr, this);
        mPageReferenceMap.put(position, myFragment);
        return myFragment;

    }

    public void fillList(List list) {

        ((TabBasic)myFragment).reInit();

        for(Card card: list.getCards()){
            ((TabBasic)myFragment).addCardToView(card.getName(), card.getId());
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }


    public Fragment getFragment(int key) {
        return mPageReferenceMap.get(key);
    }

}
