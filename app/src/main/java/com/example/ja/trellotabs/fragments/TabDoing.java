package com.example.ja.trellotabs.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.ja.trellotabs.R;

public class TabDoing extends TabBasic {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_doing, container, false);

        setListNumber("5586d6324cbc8a194be79707");

        rg = (RadioGroup) v.findViewById(R.id.radio_group);

        return v;
    }

}
