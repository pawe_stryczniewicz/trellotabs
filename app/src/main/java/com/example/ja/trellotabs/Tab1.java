package com.example.ja.trellotabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

public class Tab1 extends TabBasic {


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_1,container,false);

        rg = (RadioGroup) v.findViewById(R.id.radio_group);

        return v;
    }

}
