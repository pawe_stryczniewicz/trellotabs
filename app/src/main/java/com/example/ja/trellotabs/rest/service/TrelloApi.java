package com.example.ja.trellotabs.rest.service;

import com.example.ja.trellotabs.rest.model.Board;
import com.example.ja.trellotabs.rest.model.Card;
import com.example.ja.trellotabs.rest.model.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by Ja on 2015-07-16.
 */
public interface TrelloApi {

    String applicationKey = "9d5813c2dc048eb3d81fb746d79bd584";
    String authToken = "f4e40689e6eb8dfea43ac824451a2b5f2ef238be15bc469c854d7f82070f097f";

    @GET("/1/boards/{boardNr}?lists=open&list_fields=name&fields=name&key=" + applicationKey)
    void getBoard(@Path("boardNr") String param, Callback<Board> response);

    @GET("/1/lists/{listNr}?fields=name&cards=open&card_fields=name&key=" + applicationKey)
    void getList(@Path("listNr") String param, Callback<List> response);

    @POST("/1/lists/5586d630b51a32a3375c7bc0/cards?key=" + applicationKey + "&token=" + authToken)
    void setCard(@Body Card card, Callback<Card> cb);

    @PUT("/1/cards/{cardId}/idList?value={listId}" + "&key=" + applicationKey + "&token=" + authToken)
    void moveCard(@Path("cardId") String cardId, @Path("listId") String listId, Callback<Card> cb);

}
