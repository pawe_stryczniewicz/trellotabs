package com.example.ja.trellotabs.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.ja.trellotabs.R;

public class TabToDo extends TabBasic {


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_to_do,container,false);

        setListNumber("5586d630b51a32a3375c7bc0");

        rg = (RadioGroup) v.findViewById(R.id.radio_group);

        return v;
    }

}
