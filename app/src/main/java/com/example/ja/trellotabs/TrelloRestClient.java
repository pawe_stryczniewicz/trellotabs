package com.example.ja.trellotabs;

import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@EBean
public class TrelloRestClient {

    static ArrayList<String> toDoCardsArray = new ArrayList<>();
    static ArrayList<String> doingCardsArray = new ArrayList<>();
    static ArrayList<String> doneCardsArray = new ArrayList<>();

    static String trelloApiAddress = "https://api.trello.com";
    static String idToDo = "5586d630b51a32a3375c7bc0";
    static String idDoing = "5586d6324cbc8a194be79707";
    static String idDone = "5586d633f14bea770fc1b14c";

    static String myBoardId = "tQTRTVDF";
    static String developerKey = "9d5813c2dc048eb3d81fb746d79bd584";
    static String authToken = "f4e40689e6eb8dfea43ac824451a2b5f2ef238be15bc469c854d7f82070f097f";

    static String myBoardUrl = trelloApiAddress + "/1/board/tQTRTVDF?key=" + developerKey + "&token=" + authToken;
    static String defaultTrelloBoardUrl = trelloApiAddress + "/1/members/me?key=" + developerKey + "&token=" + authToken;
    static String trelloBoard = trelloApiAddress + "/1/board/4d5ea62fd76aa1136000000c?key=" + developerKey;



    private ViewPagerAdapter adapter = null;
    private ViewPager pager = null;
    private MainActivity mainActivity;


    public void injectObjects(MainActivity mainActivity, ViewPagerAdapter adapter, ViewPager pager){
        this.adapter = adapter;
        this.pager = pager;
        this.mainActivity = mainActivity;

    }

    @Background
    void postAction(String cardName){

        String addCard = trelloApiAddress + "/1/lists/5586d630b51a32a3375c7bc0/cards?name=" + cardName + "&desc=ooo&labels=blue&key=" + developerKey + "&token=" + authToken;

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String response = restTemplate.postForObject(addCard, null, String.class);

        String matchedId = response.substring( response.indexOf(":\"")+2,response.indexOf(",")-1);

        toDoCardsArray.add(matchedId);

        mainActivity.updateToDoUi();
    }

    @Background
    void putAction(int selectedId, int srcTab, int destTab){
        RestTemplate restTemplate = new RestTemplate();

        String destListId = destTab == 0 ? idToDo : (destTab == 1 ? idDoing : idDone);

        String moveCard = trelloApiAddress + "/1/cards/" + toDoCardsArray.get(selectedId-1) + "/idList?value=" + destListId + "&key=" + developerKey + "&token=" + authToken;

        restTemplate.put(moveCard, null);


        if(destTab == 0) {
            mainActivity.updateToDoUi();
        }
        else if (destTab == 1) {
            mainActivity.updateDoingUi();
        }
        else {
            mainActivity.updateDoneUi();
        }

        if(srcTab!=destTab && !(srcTab == 0 && destTab == 2 )) {
            mainActivity.removeCardView(selectedId - 1, srcTab);
        }

    }

    @Background
    void deleteAction(int selectedId, int srcDeleteTab){

        String deleteCard = trelloApiAddress + "/1/cards/" + toDoCardsArray.get(selectedId-1)  + "?key=" + developerKey + "&token=" + authToken;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(deleteCard);

        toDoCardsArray.remove(selectedId - 1);

        mainActivity.removeCardView(selectedId-1, srcDeleteTab);

    }

}
