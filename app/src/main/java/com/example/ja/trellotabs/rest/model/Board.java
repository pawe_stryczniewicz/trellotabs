package com.example.ja.trellotabs.rest.model;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;

public class Board {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private java.util.List<List> lists = new ArrayList<List>();

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     *
     * @return
     * The lists
     */
    public java.util.List<List> getLists() {
        return lists;
    }

    /**
     *
     * @param lists
     * The lists
     */
    public void setLists(java.util.List<List> lists) {
        this.lists = lists;
    }

}