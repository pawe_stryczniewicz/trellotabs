package com.example.ja.trellotabs.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.example.ja.trellotabs.R;

public class TabDone extends TabBasic {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_done, container, false);

        setListNumber("5586d633f14bea770fc1b14c");

        rg = (RadioGroup) v.findViewById(R.id.radio_group);

        return v;
    }

}