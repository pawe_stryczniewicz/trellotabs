package com.example.ja.trellotabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ja.trellotabs.fragments.TabBasic;
import com.example.ja.trellotabs.fragments.TabDoing;
import com.example.ja.trellotabs.fragments.TabDone;
import com.example.ja.trellotabs.fragments.TabToDo;
import com.example.ja.trellotabs.rest.model.Card;
import com.example.ja.trellotabs.rest.model.List;
import com.example.ja.trellotabs.rest.service.TrelloApi;
import com.example.ja.trellotabs.rest.service.TrelloRestClient;
import com.google.samples.apps.iosched.ui.widget.SlidingTabLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity {

    ViewPagerAdapter adapter;
    TrelloRestClient trelloRestClient;
    CharSequence Titles[]={"To Do", "Doing", "Done"};
    int NumOfTabs = 3;

    @ViewById
    Toolbar tool_bar;

    @ViewById
    ViewPager pager;

    @ViewById
    SlidingTabLayout tabs;

    private int selectedId;
    private int cardNumber;


    @OptionsMenuItem
    public MenuItem actionAdd;

    private int srcTab;
    private int srcDeleteTab;


    @OptionsItem
    void actionAdd(){
        postAction();
    }

    @OptionsItem
    void actionCopy(){
        int index = pager.getCurrentItem();
        TabBasic tab = (TabBasic)adapter.getFragment(index);
        this.selectedId = tab.getSelectedId();

        srcTab = index;
    }

    @OptionsItem
    void actionPaste(){
        putAction();
    }

    @OptionsItem
    void actionDelete(){
        int index = pager.getCurrentItem();
        TabBasic tab = (TabBasic)adapter.getFragment(0);
        this.selectedId = tab.getSelectedId();

        srcDeleteTab = index;

        deleteAction();
    }

    @AfterViews
    void init(){
        setSupportActionBar(tool_bar);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(1);
        pager.setSaveEnabled(false);


        tabs.setDistributeEvenly(true);


        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });


        tabs.setViewPager(pager);

        trelloRestClient = new TrelloRestClient();
        trelloRestClient.injectObjects(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter =  new ViewPagerAdapter(this, getSupportFragmentManager(),Titles, NumOfTabs);

    }

    @Background
    void postAction(){
        trelloRestClient.postAction("Karta " + cardNumber++);
    }

    @Background
    void putAction(){
        int destTab = pager.getCurrentItem();
        trelloRestClient.putAction(this.selectedId, srcTab, destTab);

    }

    @Background
    void deleteAction(){
        trelloRestClient.deleteAction(this.selectedId, srcDeleteTab);

    }

    @UiThread
    public void  updateToDoUi(List list) {
        adapter.fillList(list);
    }

    @UiThread
    public void  updateDoingUi(){
        int index = pager.getCurrentItem();
        adapter = ((ViewPagerAdapter)pager.getAdapter());
        Fragment fragment = adapter.getFragment(index);

        ((TabDoing)fragment).refresh();
    }

    @UiThread
    public void  updateDoneUi(){
        int index = pager.getCurrentItem();
        adapter = ((ViewPagerAdapter)pager.getAdapter());
        Fragment fragment = adapter.getFragment(index);

        ((TabDone)fragment).refresh();
    }

    @UiThread
    public void removeCardView(int viewId, int tabIndex){

        Fragment fragment = adapter.getFragment(tabIndex);
        ((TabBasic) fragment).removeRadioGroupElement(viewId);

    }

}

