package com.example.ja.trellotabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public abstract class TabBasic extends Fragment {

    public View v;
    public RadioGroup rg;
    public int selectedId;
    public int cardNum;

    @Override
    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);


    public void refresh() {
        RadioButton rb = new RadioButton(getActivity());
        rb.setText("Karta " + cardNum);
        rb.setTag(cardNum);
        cardNum++;


        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelected(rg.getCheckedRadioButtonId());
            }
        });

        rg.addView(rb);
    }


    public void setSelected(int selected) {
        this.selectedId = selected;
    }

    public int getSelectedId(){
        return this.selectedId;
    }

    public void removeRadioGroupElement(int index){
        rg.removeView(rg.findViewWithTag(index));
    }

}
