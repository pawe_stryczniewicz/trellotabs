package com.example.ja.trellotabs.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public abstract class TabBasic extends Fragment {

    public View v;
    public RadioGroup rg;
    private int selectedId;
    private int cardNum;
    private String listNumber;

    @Override
    public abstract View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);


    public void reInit(){
        rg.removeAllViews();
    }

    public void refresh() {
        RadioButton rb = new RadioButton(getActivity());
        rb.setText("Karta " + cardNum);
        rb.setTag(cardNum);
        cardNum++;


        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelected(rg.getCheckedRadioButtonId());
            }
        });

        rg.addView(rb);
    }

    public void addCardToView(String cardName, String cardId){
        RadioButton rb = new RadioButton(getActivity());
        rb.setText(cardName);
        rb.setTag(cardId);

        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelected(rg.getCheckedRadioButtonId());
            }
        });

        rg.addView(rb);

    }


    public void setSelected(int selected) {
        this.selectedId = selected;
    }

    public int getSelectedId(){
        return this.selectedId;
    }

    public void removeRadioGroupElement(int index){
        rg.removeView(rg.findViewWithTag(index));
    }

    public String getListNumber() {
        return listNumber;
    }

    public void setListNumber(String listNumber) {
        this.listNumber = listNumber;
    }
}
